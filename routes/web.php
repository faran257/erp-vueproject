<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use PHPUnit\Framework\MockObject\Stub\ReturnStub;



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// supller crud Apis 
Route::get('/supplier','Supplier\SupplierController@index');
Route::post('/supplier/create','Supplier\SupplierController@create');
Route::post('/supplier/update/{id}','Supplier\SupplierController@update');
Route::get('/supplier/find', 'Supplier\SupplierController@search');
Route::delete('/supplier/delete/{id}','Supplier\SupplierController@destroy');

// cutomer crud Apis 
Route::get('/customer','Customer\CustomerController@index');
Route::post('/customer/create','Customer\CustomerController@create');
Route::post('/customer/update/{id}','Customer\CustomerController@update');
Route::get('/customer/find', 'Customer\CustomerController@search');
Route::delete('/customer/delete/{id}','Customer\CustomerController@destroy');

// Product crud Apis 
Route::get('/product','Product\ProductController@index');
Route::post('/product/create','Product\ProductController@create');
Route::post('/product/update/{id}','Product\ProductController@update');
Route::get('/product/find', 'Product\ProductController@search');
Route::delete('/product/delete/{id}','Product\ProductController@destroy');

// category crud Apis 
Route::get('/category','Product\CategoryController@index');
Route::post('/category/create','Product\CategoryController@create');
Route::post('/category/update/{id}','Product\CategoryController@update');
Route::get('/category/find', 'Product\CategoryController@search');
Route::delete('/category/delete/{id}','Product\CategoryController@destroy');


// brand crud Apis 
Route::get('/brand','Product\BrandController@index');
Route::post('/brand/create','Product\BrandController@create');
Route::post('/brand/update/{id}','Product\BrandController@update');
Route::get('/brand/find', 'Product\BrandController@search');
Route::delete('/brand/delete/{id}','Product\BrandController@destroy');


// group crud Apis 
Route::get('/group','Product\GroupController@index');
Route::post('/group/create','Product\GroupController@create');
Route::post('/group/update/{id}','Product\GroupController@update');
Route::get('/group/find', 'Product\GroupController@search');
Route::delete('/group/delete/{id}','Product\GroupController@destroy');

// product-variant crud Apis 
Route::get('/variant','Product\VariantController@index');
Route::post('/variant/create','Product\VariantController@create');
Route::post('/variant/update/{id}','Product\VariantController@update');
Route::get('/variant/find', 'Product\VariantController@search');
Route::delete('/variant/delete/{id}','Product\VariantController@destroy');

// Tax crud Apis 
Route::get('/tax','Product\TaxController@index');
Route::post('/tax/create','Product\TaxController@create');
Route::post('/tax/update/{id}','Product\TaxController@update');
Route::get('/tax/find', 'Product\TaxController@search');
Route::delete('/tax/delete/{id}','Product\TaxController@destroy');



// Branch crud Apis 
Route::get('/branch','Product\BranchController@index');
Route::post('/branch/create','Product\BranchController@create');
Route::post('/branch/update/{id}','Product\BranchController@update');
Route::get('/branch/find', 'Product\BranchController@search');
Route::delete('/branch/delete/{id}','Product\BranchController@destroy');