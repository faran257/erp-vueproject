/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);



import VueAxios from 'vue-axios';
import axios from 'axios';
import { Form, HasError, AlertError} from 'vform';
//Import Sweetalert2
import Swal from 'sweetalert2';
window.Swal = Swal;
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
  window.Toast = Toast

window.form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.use(VueAxios, axios);


Vue.component('login-component', require('./components/Loginform.vue').default);


let Fire = new Vue()
window.Fire = Fire;

let routes = [
    { path: '/dashboard', component: require('./components/Dashboard.vue').default },
    { path: '/suplier', component: require('./components/supplier/Supplier.vue').default },
    { path: '/customer', component: require('./components/customer/Customer.vue').default },
    { path: '/product', component: require('./components/product/Product.vue').default },
    { path: '/brand', component: require('./components/product/Brand.vue').default },
    { path: '/category', component: require('./components/product/Category.vue').default },
    { path: '/group', component: require('./components/product/Group.vue').default },
    { path: '/variant', component: require('./components/product/ProductVariant.vue').default },
    { path: '/tax', component: require('./components/product/Tax.vue').default },
    { path: '/order', component: require('./components/sale/Order.vue').default },
    { path: '/stock', component: require('./components/Product/Stock.vue').default },
    { path: '/branch', component: require('./components/Product/Branch.vue').default },

  ]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  });

  const app = new Vue({
    router,
    data:{
        search: ''
    },
    methods:{
        searchit: _.debounce(() => {
            Fire.$emit('searching');
        },1000),

        printme() {
            window.print();
        }
    }
}).$mount('#app');