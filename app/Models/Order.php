<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'order_status',
        'order_date',
        'required_date',
        'shipping_date',
        'branch_id',
        'supplier_id'
    ];
    
     /**
     * Get the customers.
     */
    public function customers()
    {
        return $this->hasMany(Customer::class);
    }
     /**
     * Get the branches.
     */
    public function branches()
    {
        return $this->hasMany(Branch::class);
    }
    /**
     * Get the branches.
     */
    public function suppliers()
    {
        return $this->hasMany(Supplier::class);
    }
}
