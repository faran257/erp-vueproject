<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $table = 'product_taxes';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
     /**
     * Get the Product .
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
