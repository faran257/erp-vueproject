<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','phone','email','street','city','state','zipcode'];
    /**
     * Get the Product .
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
    /**
     * Get the Stock .
     */
    public function stock()
    {
        return $this->belongsTo(Stcok::class);
    }
      /**
     * Get the Order .
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
