<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
      /**
     * Get the Stock .
     */
    public function stock()
    {
        return $this->belongsTo(Stcok::class);
    }
}
