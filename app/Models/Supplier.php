<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','email','mobile','address','company',
    ];
      /**
     * Get the Order .
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
