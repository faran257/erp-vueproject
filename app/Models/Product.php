<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','image','category_id','brand_id','group_id','varient_id','branch_id','product_type_id','product_tax_id',
    ];
    /**
     * Get the categories.
     */
    public function categories()
    {
        return $this->hasMany(category::class);
    }
     /**
     * Get the brands.
     */
    public function brands()
    {
        return $this->hasMany(Brand::class);
    }
     /**
     * Get the branches.
     */
    public function branches()
    {
        return $this->hasMany(Branch::class);
    }
    /**
     * Get the groups.
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }
    /**
     * Get the productPrices.
     */
    public function productPrices()
    {
        return $this->hasMany(ProductPrice::class);
    }
    /**
     * Get the productTypes.
     */
    public function productTypes()
    {
        return $this->hasMany(ProductType::class);
    }
    /**
     * Get the ProductTax.
     */
    public function productTaxes()
    {
        return $this->hasMany(Tax::class);
    }
    /**
     * Get the productVariants.
     */
    public function productVariants()
    {
        return $this->hasMany(ProductVariant::class);
    }
    /**
     * Get the Stock .
     */
    public function stock()
    {
        return $this->belongsTo(Stcok::class);
    }
}
