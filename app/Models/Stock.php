<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['branch_id','quantity','product_id','discount_id'];
     /**
     * Get the Product .
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }
    /**
     * Get the Product .
     */
    public function branches()
    {
        return $this->hasMany(Branch::class);
    }
     /**
     * Get the discount .
     */
    public function discounts()
    {
        return $this->hasMany(Discount::class);
    }
}
