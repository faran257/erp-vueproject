<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    protected $table = 'product_variants';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','sku','product_price_id'];
     /**
     * Get the Product .
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    /**
     * Get the Product Price .
     */
    public function prices()
    {
        return $this->hasMany(ProductPrice::class,'id');
    }
}
