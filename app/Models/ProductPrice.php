<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['purchase_price','sale_price'];

    /**
     * Get the Product .
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get the Productvariant .
     */
    public function variant()
    {
        return $this->belongsTo(ProductVariant::class);
    }

}
