<?php

namespace App\Traits;
use Intervention\Image\ImageManagerStatic as Image;

trait ImageUploadTrait
{
    public function imageUpload($request)
    {
        if ( ! $request->hasFile($this->imageName)) {
            return false;
        }
        $avatar = $request->file($this->imageName);
        $filename = time(). '.'.$avatar->getClientOriginalExtension();
        Image::make($avatar)->resize(300,300)->save(public_path('upload/'.$filename));
        $file_path = '/upload/'.$filename;
        return $file_path;
    }
}