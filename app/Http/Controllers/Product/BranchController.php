<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branch;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return Branch::all();
    	
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return  Branch::create($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $branch      = Branch::findOrFail($id);
        $inputs     = $request->all();
        if ($branch && $this->validation($inputs)) {
          return  $branch->update($inputs);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branch::findOrFail($id);
        $branch->delete();
        return ['message' => 'Supplier Deleted'];
    }
    /**
     * Validate the request 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validation($inputs)
    {
        return [
             'name'     =>'required',
             'phone'    =>'required',
             'email'    =>'required',
             'street'   =>'required',
             'city'     =>'required',
             'state'    =>'required',
             'zipcode'  =>'required'
        ];
    }
     /**
     * Category Searrch data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(){
        if ($search   = \Request::get('q')) {
               $branch = Branch::where(function($query) use ($search){
                   $query->where('name','LIKE',"%$search%")
                           ->orWhere('email','LIKE',"%$search%");
               })->paginate(10);
           }else{
            $branch = Branch::all()->paginate(10);
           }
   
           return $branch;
       }
}
