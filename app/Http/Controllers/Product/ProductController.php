<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Traits\ImageUploadTrait;
use App\Models\ProductType;

class ProductController extends Controller
{
    use ImageUploadTrait;

    protected $imageName = 'image';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $inputs = $request->all();
        if ($this->validation($inputs)) {
           if ($request->purchase_price && $request->sell_price)  {
                $productPrice =  ProductPrice::create([
                     'purchase_price' => $request->purchase_price,
                     'sale_price'     => $request->sell_price
                 ]);
           } 
           $productType  =  ProductType::create([
            'name' => $request->product_type_id
           ]);
            $product  = new Product;
            $product->fill($inputs);
            $product->product_price_id = $productPrice->id;
            $product->product_type_id  = $productType->id;
            $product->save();

            
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $products = Product::findOrFail($id);
        $inputs   = $request->all();
        if($products && $this->validation($inputs)){
          return  $products->update($inputs);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products = Product::findOrFail($id);
        $products->delete();
        return ['message' => 'Supplier Deleted'];
    }
    /**
     * Validate the request 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validation($inputs)
    {
        return [
             'name'                      =>'required',
             'image'                     =>'required|image|mimes:jpeg,png,jpg,bmp|max:2048',
             'category_id'               =>'required',
             'brand_id'                  =>'required',
             'group_id'                  =>'required',
             'branch_id'                 =>'required',
             'product_type_id'           =>'required',
             'product_tax_id'            =>'required',
             'product_price_id'          =>'required',
             'product_variant_id'        =>'required'
        ];
    }
     /**
     * Product Searrch data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(){
        if ($search = \Request::get('q')) {
               $products = Product::where(function($query) use ($search){
                   $query->where('name','LIKE',"%$search%")
                           ->orWhere('email','LIKE',"%$search%");
               })->paginate(10);
           }else{
               $products = Product::all()->paginate(10);
           }
   
           return $products;
       }

}
