<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tax;

class TaxController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return Tax::all();
    	
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $inputs = $request->all();
        if ($this->validation($inputs)) {
            return  Tax::create($inputs);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $taxes      = Tax::findOrFail($id);
        $inputs     = $request->all();
        if ($taxes && $this->validation($inputs)) {
          return  $taxes->update($inputs);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $taxes = Tax::findOrFail($id);
        $taxes->delete();
        return ['message' => 'Group Deleted'];
    }
    /**
     * Validate the request 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validation($inputs)
    {
        return [
             'name'=>'required'
        ];
    }
     /**
     * Category Searrch data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(){
        if ($search   = \Request::get('q')) {
               $taxes = Tax::where(function($query) use ($search){
                   $query->where('name','LIKE',"%$search%")
                           ->orWhere('email','LIKE',"%$search%");
               })->paginate(10);
           }else{
               $taxes = Tax::all()->paginate(10);
           }
   
           return $taxes;
       }
}
