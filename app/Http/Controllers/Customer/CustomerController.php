<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Customer::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $inputs = $request->all();
        if ($this->validation($inputs)) {
            return  Customer::create($inputs);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $inputs   = $request->all();
        if($customer && $this->validation($inputs)){
          return  $customer->update($inputs);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();
        return ['message' => 'Supplier Deleted'];
    }

    /**
     * Validate the request 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validation($inputs)
    {
        return [
             'first_name'   =>'required|alphanum|min:3',
             'last_name'    =>'required|alphanum|min:3',
             'email'        =>'required|unique:supplier,email',
             'mobile'       =>'required|min:10|max:10',
             'address'      =>'required|alphanum',
             'company'      =>'required|alphanum'
        ];
    }

    
    /**
     * Supplier Searrch data
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(){
     if ($search = \Request::get('q')) {
            $customer = Customer::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%")
                        ->orWhere('email','LIKE',"%$search%");
            })->paginate(10);
        }else{
            $customer = Customer::all()->paginate(10);
        }

        return $customer;
    }
}
